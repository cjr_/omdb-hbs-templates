# OMDB Search with AJAX

Update `app.js` to search the OMDB API with the given search term.
Append the movie results to the page.

## OMDB API

To search for movies:

`http://www.omdbapi.com/?s=movie title here`

## Tips

* Use `http-server` from inside this directory and open the URL in your browser
* Read the [OMDB API Documentation](http://omdbapi.com/)
* Remember to prevent the default action on the form.

## Stretch

* Using the imdb id, add a link to the IMDB page.
* When a movie poster is clicked, make a request to get more details about the movie.
  * Append the details below the search box.
  * Replace the details when a new movie poster is clicked
  * Clear the details when a new search term is entered
