// Add a click event handler to the search button

$(document).ready(createClickHandler);

function createClickHandler() {
  $('form').submit(search);
}

function search(event) {
  event.preventDefault();
  var searchText = $('input').val().trim();
  var APIUrl = 'http://www.omdbapi.com/?s=' + searchText;
  $.get(APIUrl, showMovies);
}

function showMovies(result) {
  var $movies = $('.movies');
  $movies.html('');
  // for(var movie of result.Search) {
  //   console.log(movie);
  //   var $movieDiv = createMovieWithTemplate(movie);
  //   $movies.append($movieDiv);
  // }

  // console.log(result);
  var $results = createMoviesWithTemplate(result);
  $movies.append($results);
}

function createMovie(movie) {
  var movieHTML = '<div><h2>' + movie.Title + '</h2><h3>' + movie.Year + '</h3><img src="' + movie.Poster + '"></img></div><hr />';
  return $(movieHTML);
}

function showMoviesWithTemplate(result) {
  var $movies = $('.movies');
  $movies.html('');
  var $results = createMoviesWithTemplate(result);
  $movies.append($results);
}

function createMovieWithTemplate(movie) {
  var source   = $("#movie-template").html();
  var template = Handlebars.compile(source);
  var templateHTML = template(movie);
  return $(templateHTML);
}

function createMoviesWithTemplate(result) {
  var source   = $("#movies-template").html();
  var template = Handlebars.compile(source);
  var templateHTML = template(result);
  return $(templateHTML);
}
